#! /usr/bin/env python
# coding: utf-8

import unittest
from src.math.matrix import Matrix

class Matrix__str__Test(unittest.TestCase):
    """math.Matrix.__str__()のテスト"""
    
    def test(self):
        """[正常系]Matrixの文字列表現を確認する"""
        mat = Matrix([[0.0, 0.1, 0.2],
                      [1.0, 1.1, 1.2]])

        expected = "[0.0, 0.1, 0.2]\n[1.0, 1.1, 1.2]"
        actual = str(mat)

        self.assertEqual(expected, actual)

if __name__ == "__main__":
    unittest.main()
