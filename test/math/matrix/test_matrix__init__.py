#! /usr/bin/env python
# coding: utf-8

import unittest
from src.math.matrix import Matrix

class Matrix__init__Test(unittest.TestCase):
    """math.Matrix.__init__()のテスト"""
    
    def test_arg_elems(self):
        """[正常系]引数に2次元リストを渡した場合、エラーが起こらないことを確認する"""
        Matrix([[0.0, 0.1, 0.2],
                [1.0, 1.1, 1.2]])

    def test_arg_row_col(self):
        """[正常系]引数に行数と列数を渡した場合、エラーが起こらないことを確認する"""
        Matrix(row=2, col=3)

if __name__ == "__main__":
    unittest.main()
