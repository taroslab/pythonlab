#! /usr/bin/env python
# coding: utf-8

class Matrix:
    """行列クラス"""

    __slots__ = ['__elems', '__row', '__col']

    def __init__(self, elems=[], row=0, col=0):
        """コンストラクタ。
        行列の生成方法は以下の2種類。
        ・要素の指定: Matrix([[0.0, 0.1, 0.2], [1.1, 1.2, 1.3]])
        ・行数と列数の指定: Matrix(row=2, col=3)

        Args:
            elems (double[][]): 行列の要素
            row (int): 行列の行数
            col (int): 行列の列数
        """
        self.__elems = elems if elems else [[0.0 for j in range(col)] for i in range(row)]
        self.__row = len(self.__elems)
        self.__col = len(self.__elems[0])
    
    def __str__(self):
        return "\n".join([str(row_vec) for row_vec in self.__elems])
    
    # keyはタプル。elem = matrix[1, 2] のように取得する
    def __getitem__(self, key):
        return self.__elems[key[0]][key[1]]
    
    # keyはタプル。matrix[1, 2] = value のように設定する
    def __setitem__(self, key, elem):
        self.__elems[key[0]][key[1]] = elem

    def get_row(self):
        return self.__row
    
    row = property(get_row)

    def get_col(self):
        return self.__col
    
    col = property(get_col)
    
    def get_elems(self):
        return self.__elems
    
    elems = property(get_elems)
