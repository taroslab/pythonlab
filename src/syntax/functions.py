#! /usr/bin/env python
# coding: utf-8

# 関数

def sayHello():
    # ローカル変数。関数内のブロック内でのみ有効
    msg = "Hello"
    print(msg)

# 引数はカンマ(,)で区切る
# 引数にはデフォルト引数を与えられる。
# デフォルト値を持った引数を、デフォルト値を持たない引数の前に書くことはできない。
def add(a, b=0):
    # 値を戻す場合はreturnを書く
    return a + b

# 関数のコールはカッコ()をつける
sayHello()

# 引数ありの場合。この場合、aは1、bは2になる
a = add(1, 2)
b = add(4)

# 引数のキーワードを指定することも可能。
# キーワード指定をキーワード指定無しの前に書くことはできない。
c = add(b=2, a=3)
h = add(2, b=8) # add(a=8, 2)は構文エラー
d = add(a=6)

# 関数名にカッコを付けないで記述すると、関数をオブジェクトとして扱える
g = sayHello
g() # 代入した関数のコール

# ラムダ式
# lambda 引数: 式
# 引数が無い場合は、
# lambda: 式
e = lambda: print("World")
e() # 関数のコール

f = lambda a, b: a + b
ret = f(20, 30)

if __name__ == "__main__":
    print(a)
    print(b)
    print(c)
    print(d)
    print(ret)
