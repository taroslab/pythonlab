#! /usr/bin/env python
# coding: utf-8

# 演算子

# 算術演算子
a0 = +1
a1 = -1
a2 = 1 + 1
a3 = 9 - 2
a4 = 2 * 3
a5 = 5 / 2 # 結果は浮動小数点数
a6 = 5 // 2 # 整除算(除算の整数部を返す)
a7 = 5 % 2 # 除算の余りを返す
a8 = 2 ** 3 # 累乗(2の3乗)

# ビット演算子
b0 = ~0b1001 # ビット反転
b1 = 0b1110 & 0b1001 # 論理積
b2 = 0b1110 | 0b1001 # 論理和
b3 = 0b1110 ^ 0b1001 # 排他的論理和
b4 = 0b1110 << 1 # ビット左シフト(算術シフト)
b5 = 0b1110 >> 1 # ビット右シフト(算術シフト)

# 代入演算子
c = 10
c += 1
c -= 1
c *= 2
c /= 2
c %= 3
c **= 1

c1 = 0b1110
c1 //= 1
c1 |= 1
c1 ^= 1
c1 <<= 1
c1 >>= 1

# 論理演算子
d = True and False
d = True or False
d = not True

# 条件演算子
# conditionがTrueなら1、Falseなら10を返す
condition = True
e = 1 if condition else 10

if __name__ == "__main__":
    print(a1)
