#! /usr/bin/env python
# coding: utf-8

# 制御構文

# if文
# if condition:
#     ...block...
if True:
    print("if statement")

if False:
    pass
else:
    print("else statement")

if False:
    pass
elif True:
    print("elif statement")
else:
    # else文は省略可
    pass

# for文
# for variable in sequence
#     ...block...
# sequenceにはイテレータブルなオブジェクトを指定。
# range([開始,] 終了[, ステップ])
for i in range(5): # 0～4まで繰り返す
    print(i)

# while文
# while condition:
#     ...block...
a = "Hi"
while True:
    print(a)
    if a == "Hi":
        a = "Bye"
        continue
    else:
        break
