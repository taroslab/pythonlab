#! /usr/bin/env python
# coding: utf-8

# 型

# ブール型
# リテラルはTrueとFalse
flag = True
flag = False

# 整数型
# 10進数、2進数、8進数、16進数がある
d = 10
b = 0b1001
o = 0o017
h = 0x01AF

# 浮動小数点型
f = 3.14
d = 1.0e-10

# 文字列型
# シングルクオーテーション(')かダブルクオーテーション(")で囲む
sq = 'single-quotation'
dq = "double-quotation"

# raw文字列
# エスケープシーケンスはコントロールコードなどに変換されず、そのまま文字として扱われる
rawStr = r"C:\workspace\hoge.py"

# リスト
lis = [1, 2, "hoge", ["huga", "piyo"]]

# タプル
# 変更不可なリスト
tap = (0, 1, "hoge")

# ディクショナリ
# キー:値のペア
dic = {'spam':"スパム", 'hum':"ハム", 1:"one"}

# セット
# 重複不可なディクショナリ
st = {"スパム", "ハム", 1}

if __name__ == "__main__":
    print(flag)
    print(d)
    print(b)
    print(o)
    print(h)
    print(f)
    print(d)
    print(sq)
    print(dq)
    print(rawStr)
    print(lis)
    print(tap)
    print(dic)
    print(st)
