#! /usr/bin/env python
# coding: utf-8

# クラス
class Emploee:
    """従業員クラス"""

    # 当クラスに定義できるアトリビュート(属性、メンバ変数)一覧
    __slots__ = ['__name' ,'__age']

    # コンストラクタ
    def __init__(self, name, age):
        # アトリビュートの実体は代入して作成しなければならない。
        # メンバをprivateにする場合は識別子の頭に__(アンダースコア2つ)を付ける。
        self.__name = name
        self.__age = age
    
    # デストラクタ
    def __del__(self):
        print("called destructor")
    
    def __str__(self):
        return "{name}({age})".format(name=self.__name, age=self.__age) 

if __name__ == "__main__":
    a = Emploee("hoge", 23)
    print(str(a))

    def f():
        b = Emploee("huga", 11)
        print(str(b))
    
    f()
    print(str(a))
    print(str(a))

