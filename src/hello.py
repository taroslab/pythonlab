#! /usr/bin/env python
# coding: utf-8

# Pythonファイルを実行
# python <ファイル名>.py

# Pythonインタプリタの開始
# > python
# 以下のように入力待ちになる
# >>> 

# Pythonインタプリタの終了
# - Windows [Ctrl] + [z]
# - Mac/Linux [Ctrl] + [d]
# または、以下のコマンドを打つ
# >>> exit()

print("Hello World")
